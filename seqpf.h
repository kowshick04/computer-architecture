/* 
 * File:   seqPF.h
 * Author: kowshick
 *
 * Created on November 1, 2014, 4:15 PM
 */

#ifndef SEQPF_H
#define	SEQPF_H

#include "cache.h"
void seqpf( struct cache_t *cp);
void dl2_mem_access(md_addr_t addr , int a, struct cache_t *cp );

int corelHit;
int PrefetchHit;
int PrefetchUsed;
int cZoneTaghit;
int hits;
int testing;
int flush;

/*
struct cZoneTable
{
    char *name;
    struct cZoneTag tags[1];
};
struct cZoneTag
{
    struct miss_addr_history *addr_history_head;
    md_addr_t tag;
    struct miss_addr_history *head;
    struct miss_addr_history *tail;
    struct miss_addr_history *miss_addr;
};

struct miss_addr_history
{
    struct miss_addr_history *prev_addr;
    struct miss_addr_history *next_addr;
    md_addr_t miss_addr;
    int addr_valid;
};
*/

struct cZoneTag
{
    int Tag;
    struct cZoneMissAddrBuf *head;
    struct cZoneMissAddrBuf *tail;
    int valid;
    int count;
};
struct cZoneMissAddrBuf
{
    md_addr_t miss_addr_value;
    struct cZoneMissAddrBuf *next;
    struct cZoneMissAddrBuf *prev;
};
struct cZoneTag *cZoneTable[20];
struct cZoneTag *cZonePrefetchTable[20];

int cZoneTableEmpty(struct cache_t *cp, md_addr_t addr);
int cZoneTagSearch(struct cache_t *cp, md_addr_t addr);
void cZoneupdatehistoryBuff(struct cache_t *cp, md_addr_t addr, struct cZoneTag **tag);
void cZoneTable_init(void);
void cZoneTableCreate(struct cZoneTag **temp1);
void display_cZoneTable();

void cZonePrefetchTable_init(void);
void cZonePrefetchTableCreate(struct cZoneTag **temp1);
int cZonePrefetchTagSearch(struct cache_t *cp, md_addr_t addr);
void display_cZonePrefetchTable();
void cZonepredictbuff(struct cache_t *cp, md_addr_t addr);
void cZoneupdatePrefetchBuff(struct cache_t *cp, md_addr_t addr,md_addr_t delta, struct cZoneTag **tag);
struct correlKey
{
    md_addr_t delta;
    
    struct correlKey *next;
    struct correlKey *prev;
};

struct comparison
{
    md_addr_t delta;
    
    struct comparison *next;
    struct comparison *prev;
};

struct deltabuf
{
    md_addr_t delta;
   
    struct deltabuf *next;
    struct deltabuf *prev;
    int valid;
};
struct correlKey *corelhead;
struct deltabuf *deltahead;
struct deltabuf *deltatail;
struct comparison *comparhead;
struct deltabuf *deltabufptr;
struct correlKey *correlKeyptr;
struct comparison *comparisonKeyptr;
void deltabufferflush();
int correlationHitfn();
void PushCorrelationKeyReg(md_addr_t delta);
void create_correlhist();
void PushComparisonReg(md_addr_t delta);
void prefetch_fn(struct cache_t *cp, md_addr_t addr);
void pff_test();
void pushhistorybuff(md_addr_t delta);
md_addr_t readhistorybuff();

#endif	/* SEQPF_H */